package ho.qat.seo.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

public class ReasonForTravelPage extends PageObject {

    WebElementFacade reasonToSelect;

    @FindBy(css = "#current-question > button")
    private WebElementFacade nextStepButton;


    public void selectReasonForVisit(String reason){
        String selector = null;

        switch(reason) {
            case "Work":
                selector = "#response-1+ .govuk-radios__label";
                break;
            case "JoinPartner":
                selector = "#response-4+ .govuk-radios__label";
                break;
            case "Tourism":
                selector = "#response-0+ .govuk-radios__label";
                break;
            default:
                break;
        }


     
        reasonToSelect = find(By.cssSelector(selector));

        clickOn(reasonToSelect);
    }

    public void clickNextStepButton(){
        clickOn(nextStepButton);
    }

}


