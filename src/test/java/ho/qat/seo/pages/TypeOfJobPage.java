package ho.qat.seo.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

public class TypeOfJobPage extends PageObject{
//    @FindBy(css = "#response-0+ .govuk-radios__label")
//    private WebElementFacade healthCareProfessional;
    WebElementFacade jobTypeOption;
    @FindBy(css = "#current-question > button")
    private WebElementFacade nextStepButton;

    public void selectTypeOfJob(String jobType) {
        String selector = null;
        switch (jobType) {
            case "Health and care professional":
                selector = "#response-0+ .govuk-radios__label";
                break;
            default:
                break;
        }

        jobTypeOption = find(By.cssSelector(selector));

        clickOn(jobTypeOption);
    }

    public void clickNextStepButton(){
        clickOn(nextStepButton);
    }


}
